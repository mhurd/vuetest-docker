Quickly spin up a MediaWiki instance with the [VueTest](https://gitlab.wikimedia.org/egardner/mediawiki-extensions-vuetest) extension.

## Installation 

Clone this repo:

    git clone https://gitlab.wikimedia.org/mhurd/vuetest-docker.git

## Usage

Switch to the `vuetest-docker` directory:

    cd ~/vuetest-docker

Use this command to spin up MediaWiki with the VueTest extension:
-   ```
    make freshinstallwithvuetestextension
     ```
    - Fetches the latest MediaWiki into `~/vuetest-docker/mediawiki-docker-make/mediawiki/`
    - Fetches the latest VueTest extension into `~/vuetest-docker/mediawiki-docker-make/mediawiki/extensions/VueTest`
    - Configures MediaWiki to use the VueTest extension
    - Spins up Docker containment to serve MediaWiki pages
    - Opens the VueTest extension's `Special:VueTest` page demonstrating usage of various [Codex](https://doc.wikimedia.org/codex/main/) components

See the [Makefile](https://gitlab.wikimedia.org/mhurd/vuetest-docker/-/blob/main/Makefile) for other supported make commands. They're documentation [here](https://gitlab.wikimedia.org/mhurd/mediawiki-docker-make).