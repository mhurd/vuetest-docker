SHELL := /bin/bash

-include ./mediawiki-docker-make/Makefile

.PHONY: freshinstallwithvuetestextension
freshinstallwithvuetestextension:
	-@if [ ! -f "./mediawiki-docker-make/Makefile" ]; then \
		git submodule update --init; \
	fi
	make freshinstall skipopenspecialversionpage=true;
	make applyextension extensionDirectory=VueTest extensionRepoURL=https://gitlab.wikimedia.org/egardner/mediawiki-extensions-vuetest.git wfLoadExtension=VueTest;
	./mediawiki-docker-make/utility.sh open_url_when_available "http://localhost:8080/wiki/Special:VueTest";